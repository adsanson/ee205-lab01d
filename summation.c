///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01d - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ ./summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Albert D'Sanson <adsanson@hawaii.edu>
// @date   12_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
int n = atoi(argv[1]);
n = 0;
int i;
for(i = 1; i <= atoi(argv[1]); i++)
{
n = n + i;
}
printf("The sum of the digits from 1 to %d is %d\n", atoi(argv[1]), n);
return 0;
}
